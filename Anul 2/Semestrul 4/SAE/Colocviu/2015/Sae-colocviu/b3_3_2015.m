n1=[1];
d1=[1 1 0];
n2=[2];
d2=[1 1];
te=0.2;

[nz1,dz1]=c2dm(n1,d1,te,'zoh');
[nz2,dz2]=c2dm(n2,d2,te,'zoh');
[n,d]=series(nz1,dz1,nz2,dz2);
printsys(n,d,'z');