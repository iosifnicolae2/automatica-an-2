num=[9];
den=[1 3 0];
w=logspace(-1,2);
Te=0.1;
[n,d]=c2dm(num,den,Te,'zoh');
[amp,faza]=dbode(n,d,Te,w);

subplot(2,1,1);
plot(log10(w),20*log10(amp))
subplot(2,1,2);
plot(log10(w),20*log10(faza))