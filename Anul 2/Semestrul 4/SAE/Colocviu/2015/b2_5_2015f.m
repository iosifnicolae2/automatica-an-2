function [sys, x0, str,Te] = b2_5_2015f(t,x,u,flag)
A=[1.6 -0.7;1 0];
B=[1;0];
C=[0.04 0.03];
if  flag == 2
    sys=A*x+B*u;
elseif flag==3
    sys=C*x;
elseif flag==0
    sys=[0,2,2,2,0,0,0];
    x0=[1;1];
    str=[];
    Te=[];
else
    sys=[];
end

