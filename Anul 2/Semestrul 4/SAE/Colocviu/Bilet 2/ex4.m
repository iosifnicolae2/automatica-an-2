A = [
    1 1 0.5;
    0 1 1;
    0 0 1];

B = [
    1 0;
    0 0;
    0 1];

C = [
    1 0 0;
    0 0 1];

D = [0 0;0 0];
Te = .1;

[fi, gama, C, D] = c2dm(A,B,C,D, Te, 'zoh');

a = rank(obsv(fi, C))
b = rank(ctrb(fi, gama))
% Rangurile sunt diferite de 0 atunci sistemul este observabil respectiv
% controlabil.
