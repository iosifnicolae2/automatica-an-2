function [sys, x0,str, Te] = s_function(t, x, u, flag, A, B, C, D)
 % A=[0 -0.25;1 1];
% B=[1;1];%numarul de iesiri se ia dupa nr de coloane=1
% C=[0 1];nr de intrari dupa nr de linii=1
% Te=0.1

if flag == 2
    sys = A*x+B*u;
elseif flag ==3 
    sys =C*x;
elseif flag ==0
    sys = [0,2,1,1,0,0,1];
    x0=[1;1];
    str = [];
    Te =[0.1];
else sys = [];
end
end