num = [9];
den = [1 7 0];
Te = 0.1;
w = [0:0.01:4];

[n,d] = c2dm(num, den, Te, 'zoh');

[n0, d0] = cloop(n,d);

% Vezi ca e z
[mag, phase] = freqz(n0, d0, w);

% Nu sunt sigur ca asa se converteste
M = 20*log10(abs(mag));
subplot(2, 1, 1);
plot(log10(w), M);
subplot(2, 1, 2);
dnyquist(n,d, Te, w);
