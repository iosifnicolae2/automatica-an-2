clear all;
Te = 0.3;

num = [2];
den = [1 1 0];

% nu contine ER0, deci inmultuim cu s
num = conv(num, [1 0]);

[num_z, den_z] = c2dm(num,den, Te, 'zoh');

% Inmultim cu z/(z-1)
num_z1 = [1 0];
den_z1 = [1 -1];
[n,d] = series(num_z, den_z, num_z1, den_z1);

rlocus(n,d)
zgrid
