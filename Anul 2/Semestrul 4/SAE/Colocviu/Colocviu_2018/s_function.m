function [sys, x0,str, Te] = s_function(t, x, u, flag, A, B, C, D)
if flag == 2
 sys = A*x+B*u;
elseif flag ==3
 sys =C*x;
elseif flag ==0
 sys = [0,2,1,1,0,0,1];
 x0=[1;0];
 str = [];
 Te =[0.1];
else sys = [];
end
end