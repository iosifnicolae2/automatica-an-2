Te = 0.1;

G2_num = [1];
G2_den = [1 1];

H_num = [1 3];
H_den = [1 2];

[num, den] = series(G2_num, G2_den, H_num, H_den);
[n,d] = c2dm(num,den, Te, 'zoh');
printsys(n,d, 'z')