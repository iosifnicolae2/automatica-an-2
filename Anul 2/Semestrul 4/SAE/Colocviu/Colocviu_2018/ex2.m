Te = 0.1;

num = [2];
den = [1 1 0];

% Inmultesc cu un s pentru ca nu exista ER0
num = conv(num, [1 0]);

[numz, denz] = c2dm(num, den, Te, 'zoh');

% Impartim cu z/(z-1)

[numz, denz] = series(numz, denz, [1 0], [1 -1]);

subplot(2,1,1);
rlocus(numz, denz)
subplot(2,1,2);
[numz0, denz0] = cloop(numz, denz);
[mag, w] = freqz(numz0, denz0);
plot(w/pi,20*log10(abs(mag)))
 
 